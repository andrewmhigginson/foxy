<?php

/**
 * (ɔ) Online FORMAPRO - GrCOTE7 -2022.
 */

namespace App\Controllers;

use App\Tools\Gc7;
use App\Models\User;

class UserController extends Controller
{
	

	public function index(): string
	{
		
		
		return $this->template->render('pages/index.twig') ;
	}

	public function registerForm(): string
	{
		
		
		return $this->template->render('pages/register.twig') ;
	}

	public function form()
	{
		$tasks = $this->tasks;

		unset($tasks[1]);
		// echo '<pre>';
		// var_dump($tasks);
		// echo '</pre>';

		return $tasks;
	}
}
