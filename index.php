<?php

use App\Controllers\TestController;
use App\Controllers\UserController;
use Inphinit\Teeny;

require_once './vendor/autoload.php';
require_once './vendor/inphinit/teeny/vendor/Teeny.php';

if (!session_id()) {
	session_start();
}

$app  = new Teeny();
$user = new UserController();


$app->action('GET', '/', function () use ($user) {
	return $user->index();
});

$app->action('GET', 'register', function () use ($user) {
	return $user->registerForm();
});

$app->action('POST', 'register', function() use ($user){
	return $user->create();
});


return $app->exec();